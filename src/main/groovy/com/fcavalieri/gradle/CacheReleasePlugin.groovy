package com.fcavalieri.gradle

import groovy.transform.Internal
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.file.DuplicatesStrategy
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.bundling.Zip
import groovy.xml.*

import java.nio.file.Files
import java.nio.file.StandardCopyOption
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import com.fcavalieri.gradle.S3Upload
import com.fcavalieri.gradle.S3Plugin
import com.fcavalieri.gradle.S3Extension
import com.fcavalieri.gradle.GitLabCredentialsExtension


class CacheReleasePlugin implements Plugin<Project> {
    void apply(Project project) {
        //project.getPluginManager().apply(S3Plugin.class);

        project.extensions.create("s3", S3ExtensionExt)
        project.extensions.create("cacheRelease", CacheReleaseExtension, project)
        project.tasks.register('package', Zip) {
            dependsOn "generateTaxonomyPackageMetadata"
            from 'root', 'build/metadata'
            exclude { it.file in List.of(
                    new File(project.rootDir, "root/catalog.xml"),
                    new File(project.rootDir, "root/taxonomyPackage.xml"),
                    new File(project.rootDir, "root/META-INF/catalog.xml"),
                    new File(project.rootDir, "root/META-INF/taxonomyPackage.xml")
            ) }
            duplicatesStrategy = DuplicatesStrategy.FAIL
            into project.cacheRelease.getPackageRootFolder()
            archiveFileName = project.cacheRelease.getPackageFileName()
            destinationDirectory = new File(project.buildDir, "dist")
            zip64 = true
        }
        project.tasks.register('publishToS3', S3Upload) {
            dependsOn  "package"

            key = project.s3.keyPrefix + "/" + project.cacheRelease.getPackageFileName()
            file = project.tasks.package.getOutputs().getFiles().getFiles()[0]
            overwrite = project.cacheRelease.overwrite
            cannedACL = project.cacheRelease.cannedACL
        }
        project.tasks.register("generateTaxonomyPackageMetadata", GenerateTaxonomyPackageMetadataTask)
        project.tasks.register("publishToGitLab", PublishToGitLabTask)

        project.tasks.register('publish') {
            dependsOn  "publishToS3", "publishToGitLab"
        }
    }
}

class CacheReleaseExtension {
    String packageRootFolder
    String packageFileName
    String gitlabProjectId
    String gitlabEndpoint

    String packageMetadataIdentifier
    String packageMetadataName
    String packageMetadataDescription
    String packageMetadataVersion
    String packageMetadataPublisher
    String packageMetadataPublisherURL
    String packageMetadataPublicationDate

    boolean generateHttpEntries = true
    boolean generateHttpsEntries = true
    boolean overwrite = false

    String cannedACL

    @Internal
    Project project

    CacheReleaseExtension(Project project) {
        this.project = project
    }

    String getPackageRootFolder() {
        if (packageRootFolder != null)
            return packageRootFolder
        return project.name + '-taxonomy-package-v' + project.version
    }

    String getPackageFileName() {
        if (packageFileName != null)
            return packageFileName
        return getPackageRootFolder() + '.zip'
    }

    String getGitlabProjectId() {
        if (gitlabProjectId != null)
            return gitlabProjectId
        throw new GradleException("gitlabProjectId has not been defined")
    }

    String getGitlabEndpoint() {
        if (gitlabEndpoint != null)
            return gitlabEndpoint
        return "https://gitlab.com"
    }

    String getPackageMetadataIdentifier() {
        if (packageMetadataIdentifier != null)
            return packageMetadataIdentifier
        throw new GradleException("packageMetadataIdentifier has not been defined")
    }

    String getPackageMetadataName() {
        if (packageMetadataName != null)
            return packageMetadataName
        throw new GradleException("packageMetadataName has not been defined")
    }

    String getPackageMetadataDescription() {
        if (packageMetadataDescription != null)
            return packageMetadataDescription
        throw new GradleException("packageMetadataDescription has not been defined")
    }

    String getPackageMetadataVersion() {
        if (packageMetadataVersion != null)
            return packageMetadataVersion
        return project.version
    }

    String getPackageMetadataPublisher() {
        if (packageMetadataPublisher != null)
            return packageMetadataPublisher
        return "Reportix"
    }

    boolean isGenerateHttpEntries() {
        return generateHttpEntries;
    }

    boolean isGenerateHttpsEntries() {
        return generateHttpsEntries;
    }

    String getPackageMetadataPublisherURL() {
        if (packageMetadataPublisherURL != null)
            return packageMetadataPublisherURL
        return "https://www.reportix.com"
    }
}

class S3ExtensionExt extends S3Extension {
    String keyPrefix
}

class GenerateTaxonomyPackageMetadataTask extends DefaultTask {
    @TaskAction
    void generateTaxonomyPackageMetadata() {
        Files.createDirectories(new File(project.buildDir, "metadata/META-INF").toPath())

        File manualCatalog = new File(project.rootDir, "root/META-INF/catalog.xml");
        File generatedCatalog = new File(project.buildDir, "metadata/META-INF/catalog.xml");
        if (manualCatalog.isFile()) {
            logger.quiet("Copying catalog")
            Files.copy(manualCatalog.toPath(), generatedCatalog.toPath(), StandardCopyOption.REPLACE_EXISTING)
        } else {
            logger.quiet("Generating catalog")
            generatedCatalog.withWriter { writer ->
                def xml = new MarkupBuilder(new IndentPrinter(writer, "  ", true))
                xml.doubleQuotes = true
                xml.mkp.xmlDeclaration(version: '1.0', encoding: 'UTF-8')
                xml.mkp.comment("AUTO-GENERATED FILE.  DO NOT MODIFY. This file is generated by the generateTaxonomyPackageMetadata gradle task.")
                xml.mkp.yield('\n')
                xml.catalog('xmlns': "urn:oasis:names:tc:entity:xmlns:xml:catalog") {
                    if (project.cacheRelease.isGenerateHttpEntries()) {
                        Files.list(new File(project.rootDir, "root").toPath()).forEach(
                                path -> rewriteURI(uriStartString: "http://" + path.getFileName() + "/", rewritePrefix: "../" + path.getFileName() + "/")
                        )
                    }
                    if (project.cacheRelease.isGenerateHttpsEntries()) {
                        Files.list(new File(project.rootDir, "root").toPath()).forEach(
                                path -> rewriteURI(uriStartString: "https://" + path.getFileName() + "/", rewritePrefix: "../" + path.getFileName() + "/")
                        )
                    }
                }
            }
        }

        File manualTaxonomyPackage = new File(project.rootDir, "root/META-INF/taxonomyPackage.xml");
        File generatedTaxonomyPackage = new File(project.buildDir, "metadata/META-INF/taxonomyPackage.xml");
        if (manualTaxonomyPackage.isFile()) {
            logger.quiet("Copying taxonomy package")
            Files.copy(manualTaxonomyPackage.toPath(), generatedTaxonomyPackage.toPath(), StandardCopyOption.REPLACE_EXISTING)
        } else {
            logger.quiet("Generating taxonomy package")
            generatedTaxonomyPackage.withWriter { writer ->
                def xml = new MarkupBuilder(new IndentPrinter(writer, "  ", true))
                xml.doubleQuotes = true
                xml.mkp.xmlDeclaration(version: '1.0', encoding: 'UTF-8')
                xml.mkp.comment("AUTO-GENERATED FILE.  DO NOT MODIFY. This file is generated by the generateTaxonomyPackageMetadata gradle task.")
                xml.mkp.yield('\n')
                xml.taxonomyPackage('xmlns': "http://xbrl.org/2016/taxonomy-package", "xml:lang": "en") {
                    identifier(project.cacheRelease.getPackageMetadataIdentifier())
                    name(project.cacheRelease.getPackageMetadataName())
                    xml.description(project.cacheRelease.getPackageMetadataDescription())
                    xml.version(project.cacheRelease.getPackageMetadataVersion())
                    publisher(project.cacheRelease.getPackageMetadataPublisher())
                    publisherURL(project.cacheRelease.getPackageMetadataPublisherURL())
                    publicationDate(project.cacheRelease.getPackageMetadataPublicationDate())
                }
            }
        }
    }
}

class PublishToGitLabTask extends DefaultTask {

    PublishToGitLabTask() {
        super()
        dependsOn = List.of("package")
    }

    @TaskAction
    void publishCache() {
        def creds = new GitLabCredentialsExtension(project)
        def url = new URL(project.cacheRelease.getGitlabEndpoint() + "/api/v4/projects/" + project.cacheRelease.getGitlabProjectId() + "/packages/generic/" + project.name +  "/" + project.version + "/" + project.cacheRelease.getPackageFileName())
        logger.info("Release URL: " + url)
        if (!project.cacheRelease.overwrite) {
            def head = url.openConnection()
            head.setDoInput(true)
            creds.apply(head)
            def headRC = head.getResponseCode();
            if (headRC.equals(200)) {
                throw new GradleException("A release for this version already exists")
            } else if (headRC.equals(404)) {
                logger.info("Does not exist")
            } else {
                throw new GradleException("Unable to verify if a release for this version already exists, status code: " + headRC)
            }
        }

        def put = url.openConnection()
        put.setDoOutput(true)
        put.setRequestMethod('PUT')
        creds.apply(put)
        Files.copy(project.tasks.package.getOutputs().getFiles().getFiles()[0].toPath(), put.outputStream)
        def postRC = put.getResponseCode();
        if (!postRC.equals(201)) {
            throw new GradleException("Upload failure: " + put.getInputStream().getText())
        }
        logger.quiet("Uploaded ${url.toString()}")
    }
}
