/*
 * This Groovy source file was generated by the Gradle 'init' task.
 */
package com.fcavalieri.gradle

import org.gradle.api.tasks.bundling.Zip
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import spock.lang.Specification

class CacheReleasePluginTest extends Specification {
    def "plugin registers task"() {
        given:
        def project = ProjectBuilder.builder().build()

        when:
        project.plugins.apply("com.fcavalieri.gradle.cacherelease")

        then:
        project.extensions.findByName("s3") != null
        project.extensions.findByName("cacheRelease") != null

        project.tasks.findByName("publishToS3") != null
        project.tasks.findByName("publishToGitLab") != null
        project.tasks.findByName("generateTaxonomyPackageMetadata") != null
        project.tasks.findByName("package") != null
        project.tasks.findByName("publish") != null
    }
}
