# Gradle Gitlab Credentials

## Usage

settings.gradle
```groovy
pluginManagement {
    repositories {
        maven { url "https://gitlab.com/api/v4/projects/33704073/packages/maven" }
        gradlePluginPortal()
    }
}
```

build.gradle
```groovy
plugins {
    id "com.fcavalieri.gradle.cacherelease" version 'X.X.X'
}
```

Usage:

```groovy
s3 {
   region = 'eu-west-1'
   bucket = 'download.reportix.com'
   keyPrefix = "cachePacks"
}

cacheRelease {
   gitlabEndpoint = "https://gitlab.reportix.com"
   gitlabProjectId = "14087168"

   packageMetadataIdentifier = "https://www.reportix.com/edinet-fsa-jp-taxonomy-package"
   packageMetadataName = "EDINet XBRL Taxonomies"
   packageMetadataDescription = "This Taxonomy Package contains taxonomies published by the Financial Services Agency of Japan (FSA) (from https://disclosure.edinet-fsa.go.jp/)."      
}
```

Optional parameters in `cacheRelease`:
* overwrite
* packageRootFolder
* packageFileName
* packageMetadataVersion
* packageMetadataPublisher
* packageMetadataPublisherURL
* packageMetadataPublicationDate

Defined tasks:
* generateTaxonomyPackageMetadata
* package
* publishCacheToS3
* publishCacheToGitLab
* publish

## Credentials

* S3 credentials are loaded in order from (the usual way, as in: https://docs.aws.amazon.com/AWSJavaSDK/latest/javadoc/com/amazonaws/auth/DefaultAWSCredentialsProviderChain.html):
  * Environment Variables - `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`
  * Java System Properties - `aws.accessKeyId` and `aws.secretKey`
  * Web Identity Token credentials from the environment or container
  * Credential profiles file at the default location (`~/.aws/credentials`) shared by all AWS SDKs and the AWS CLI
  * Credentials delivered through the Amazon EC2 container service if `AWS_CONTAINER_CREDENTIALS_RELATIVE_URI` environment variable is set and security manager has permission to access the variable,
  * Instance profile credentials delivered through the Amazon EC2 metadata service 
* GitLab credentials are loaded in order from (the usual way, as in https://gitlab.com/fcavalieri/gradle-gitlab-credentials):
  * Gradle project property `gitLabPrivateToken`
  * Gradle global property `gitLabPrivateToken` (`~/.gradle/gradle.properties`)
  * Gradle project property `gitLabDeployToken`
  * Gradle global property `gitLabDeployToken` (`~/.gradle/gradle.properties`)
  * Gradle project property `gitLabJobToken`
  * Gradle global property `gitLabJobToken` (`~/.gradle/gradle.properties`)
  * Environment variable `PRIVATE_TOKEN`
  * Environment variable `DEPLOY_TOKEN`
  * Environment variable `CI_JOB_TOKEN`